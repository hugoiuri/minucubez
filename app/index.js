angular.module('minucubez', ['ngMaterial', 'ngRoute'])
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: '/views/index.html',
                    controller: 'rankingCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }
    ])
    .config(['$locationProvider',
        function($locationProvider) {
            $locationProvider.html5Mode(false);
        }
    ])
    .config([
        '$mdThemingProvider',
        function($mdThemingProvider) {
            $mdThemingProvider.theme('default')
                .primaryPalette('blue')
                .accentPalette('teal')
                .warnPalette('red');
        }
    ]);
