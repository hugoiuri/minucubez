angular.module('minucubez').controller('rankingCtrl', ['$scope','$http', 'peopleSvc', function($scope,$http, peopleSvc){

    $scope.people = [];

    peopleSvc.list().then(function(data){
        $scope.people = data;
    }).catch(function(){
        $scope.error = "Could not fetch the data!";
    });
}]);
