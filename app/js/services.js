angular.module('minucubez').service('peopleSvc', ['$http', '$q', function($http, $q){

    var peopleSvc = {};
    peopleSvc.list = function(){
        var dfd = $q.defer();
        $http.get('/people').then(function(res){
            dfd.resolve(res.data);
        });
        return dfd.promise;
    };
    return peopleSvc;
}]);
