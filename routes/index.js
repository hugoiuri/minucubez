
var People = require('./people');

module.exports = exports = function(app, db) {

    var people = new People(db);

    app.get('/people/', people.getPeople);

}
