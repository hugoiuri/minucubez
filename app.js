var express = require('express')
  , consolidate = require('consolidate')
  , bodyParser = require('body-parser')
  , MongoClient = require('mongodb').MongoClient
  , routes = require('./routes');

var app = express();

MongoClient.connect('mongodb://sysUser:minutrade@ds047772.mongolab.com:47772/minucubez', function(err, db) {

    if(err) throw err;

    app.engine('html', consolidate.swig);
    app.set('view engine', 'html');
    //app.set('views', __dirname + '/views');
    app.use(express.static(__dirname + '/app'));

    app.use(bodyParser.urlencoded());
    app.use(bodyParser.json());

    routes(app, db);

    app.listen(process.env.PORT || 9615);
    console.log('Express server listening on port 9615');
});
